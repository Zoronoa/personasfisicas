import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './pages/login/login.component';
import { PersonaComponent } from './pages/persona/persona.component';
import { PersonasComponent } from './pages/personas/personas.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { HomeComponent } from './pages/home/home.component';
import { ReporteComponent } from './pages/reporte/reporte.component';
import { ReportesComponent } from './pages/reportes/reportes.component';

const routes: Routes = [

  {path: 'home', component: HomeComponent},
  {path: 'personas', component: PersonasComponent },
  {path: 'persona/:id', component: PersonaComponent},
  {path: 'reporte', component: ReporteComponent},
  {path: 'reportes', component: ReportesComponent},
  {path: 'registro', component: RegistroComponent},
  {path: 'login', component: LoginComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'login'}

];

@NgModule({
  imports: [
    RouterModule.forRoot( routes )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
