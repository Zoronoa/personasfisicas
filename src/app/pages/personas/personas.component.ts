import { Component, OnInit } from '@angular/core';
import { PersonasService } from '../../services/personas.service';
import { PersonaModel } from '../../models/persona.model';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {

  personas: PersonaModel[] = [];
  cargando = false;
  fileName = 'ExcelReporte.xlsx';

  constructor( private personasService: PersonasService ) { }

  ngOnInit() {

    this.cargando = true;
    //Mostrar información de personas fisicas
    this.personasService.getPersonas()
      .subscribe( resp => {
        this.personas = resp;
        this.cargando = false;
      });
  }

    //Exportar a excel
    exportexcel():void{
      let element = document.getElementById('tablaRporte');
      const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

      const wb: XLSX.WorkBook = XLSX.utils.book_new()
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      XLSX.writeFile(wb, this.fileName);
    }

    //Borrar registro
    borrarPersona( persona: PersonaModel, i: number ){

      Swal.fire({
        title: '¿Esta seguro?',
        text: `Está seguro que desea borrar el registro ${ persona.nombre }`,
        icon: 'question',
        showConfirmButton: true,
        showCancelButton: true
      }).then( resp => {

        if ( resp.value ) {
          this.personas.splice(i, 1);
          this.personasService.borrarPersona( persona.id as string).subscribe();    
        }

      });

      
    }

}
