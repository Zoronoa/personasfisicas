import { Component, OnInit } from '@angular/core';
import { ReporteService } from '../../services/reporte.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { ReporteModel } from 'src/app/models/reporte.model';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.css']
})
export class ReporteComponent {

  email = '';
  password = '';
  error: string;
  reporte = [];
  
   
  constructor(private reporteService: ReporteService, private router: Router) {
     
  }

  public submit(){
    this.reporteService.login(this.email, this.password)
      .pipe(first())
      .subscribe(
        result => {

          this.router.navigateByUrl('/reportes'); 
        
        },
        err => this.error = 'No esta autenticado',
        
      );
  }
 
}
