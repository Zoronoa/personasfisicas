import { Component, OnInit } from '@angular/core';
import { ReporteModel } from '../../models/reporte.model';
import { ReporteService } from '../../services/reporte.service';

import * as XLSX from 'xlsx';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  reportes: ReporteModel[] = [];
  cargando = false;
  public page: number;
  fileName = 'ExcelReporte.xlsx';



  constructor(private reporteService: ReporteService) { }

  ngOnInit() {
    this.cargando = true;


    this.reporteService.reportData(localStorage.getItem('access_token')).subscribe(
      data => {
      console.log("ajua",data.Data);
      this.reportes = data.Data;
      this.cargando = false;

      },
      error => {
      // Aquí se debería tratar el error, bien mostrando un mensaje al usuario o de la forma que se desee.
      Swal.fire({
        icon: 'error',
        title: 'Error al autenticar',
        text: error.message
      });
      }
    );
  }

   //Exportar a excel
   exportexcel():void{
    let element = document.getElementById('tablaRporte');
    const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

    const wb: XLSX.WorkBook = XLSX.utils.book_new()
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    XLSX.writeFile(wb, this.fileName);
  }


}
