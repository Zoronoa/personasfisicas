import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReporteService {

  uri = 'https://api.toka.com.mx/candidato/api/login/authenticate';
  uriData = 'https://api.toka.com.mx/candidato/api/customers';
  token: string;
  resultData = [];

  constructor( private http: HttpClient, private router: Router ) {
  }


  login(email: string, password: string){
    return this.http.post<{token: string}>(this.uri, {Username: email, Password: password})
      .pipe(
        map((result:any) => {
          localStorage.setItem('access_token', result.Data);
          return result;
        })
      );
  }

  reportData(token: String){
    const headers = { 'Authorization': `Bearer ${token}` }
    return this.http.get<any>(this.uriData, { headers })
}

 
  public get logIn(): boolean {
    return (localStorage.getItem('access_token') !== null);
  }
}
