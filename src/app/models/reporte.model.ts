
export class ReporteModel {

    IdCliente: string;
    FechaRegistroEmpresa: string;
    RazonSocial: string;
    RFC: string;
    Sucursal: string;
    IdEmpleado: string;
    Nombre: string;
    Paterno: string;
    Materno: string;
    IdViaje: string;

    constructor(){
        this.IdCliente = '';
        this.FechaRegistroEmpresa = '';
        this.RazonSocial = '';
        this.RFC = '';
        this.Sucursal = '';
        this.IdEmpleado = '';
        this.Nombre = '';
        this.Paterno = '';
        this.Materno = '';
        this.IdViaje = '';
    }
}